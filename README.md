# Database Model Csharp

This project have dll file you can use to fast coding, you don't need to write sql query again just use simple function like insert, update, delete...
It is inspired by laravel Eloquent ORM


# Installation

Clone or download the project, go to Database\Database\bin\Debug and get Database.dll file and add dll file into the reference of your project. In my case, i use visual studio.


# Usage

### Connection to a Database

Into your App.config file, add database configuration. For example:

> ```xml
> <connectionStrings>
>     <add name="connString" connectionString="Data Source=.;Initial Catalog=dbEmployeeDetails;Integrated Security=True" />
>     <add name="EmployeeDetails.Properties.Settings.dbEmployeeDetailsConnectionString"
>       connectionString="Data Source=AGT-TECHNIQUE;Initial Catalog=dbEmployeeDetails;Integrated Security=True"
>       providerName="System.Data.SqlClient" />
>   </connectionStrings>
> ```

### Defining Model

You will create a Model who will herite from Model class. In my case, i create Employe class as modal because the table of my database is called employe (to have clean code).

> ```c#
> using Database;
> using System;
> using System.Collections.Generic;
> using System.Data;
> using System.Linq;
> using System.Text;
> using System.Threading.Tasks;
>
> namespace EmployeeDetails
> {
>     public class Employe : Model
>     {
>         public string nom;
>         public string contact;
>     }
> }
> ```


### Instanciate Model

Now you just need to instanciate your custom model to use a different class from Model but you should also set Model connection property. It is better for you to instance model in parent class.

> ```c#
> // Form1 Constructor
> public Form1()
>         {
>             InitializeComponent();
>
>             // Set Model connection property to communicate with database
>             Model.connection = ConfigurationManager.ConnectionStrings["connString"].ConnectionString;
>
>             // Instance of my custom Model child class
>             employee = new Employe();
>             // set my table name, it is required
>             employee.tableName = "employe";
>
>             dgvEmployeeDetails.Columns[0].Visible = false;
>
>             // Example of Model method usage
>             dgvEmployeeDetails.DataSource = employee.All();
>         }
> ```

### Model Methods

#### Insert

`bool Model.Insert(Dictionary<string, string>)`

In first Dictionary string, set the name of database field and in second string, the value of the field.

The function return boolean value.

> ```c#
> var data = new Dictionary<string, string> {
>     {"name", txtEmpName.Text},
>     {"contact", txtContactNo.Text}
> };
>
> var success = employee.Insert(data);
> ```

#### Update

`bool Model.Update(int id, Dictionary<string, string>)`

In first Dictionary string, set the name of database field and in second string, the value of the field.

> ```c#
> int id = Convert.ToInt32(dgvEmployeeDetails.CurrentRow.Cells[0].Value.ToString());
> int rowIndex = dgvEmployeeDetails.CurrentRow.Index;
>
> var data = new Dictionary<string, string> {
>     {"nom", txtEmpName.Text},
>     {"contact", txtContactNo.Text}
> };
>
> var success = employee.Update(id, data);
> ```

The function return boolean value.

#### Delete

`bool Model.Update(int id, Dictionary<string, string>)`

In first Dictionary string, set the name of database field and in second string, the value of the field.

> ```c#
> int id = Convert.ToInt32(row.Cells[0].Value.ToString());
>
> var success = employee.Delete(id);
> if (!success)
>     MessageBox.Show("Error occured. Please try again...");
> ```
