﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace Database
{
    public class Model
    {
        public static string connection { get; set; }
        public string tableName { get; set; }

        public Model()
        {
            // TODO: Complete member initialization
        }

        public DataTable All()
        {
            string selectQuery = "Select * from " + tableName;
            var datatable = new DataTable();
            using (SqlConnection con = new SqlConnection(connection))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand(selectQuery, con))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(com))
                    {
                        adapter.Fill(datatable);
                    }
                }
            }

            return datatable;
        }

        public DataTable FindById(int id)
        {
            string selectQuery = "Select * from " + tableName + " where id=" + id;
            var datatable = new DataTable();
            using (SqlConnection con = new SqlConnection(connection))
            {
                con.Open();
                using (SqlCommand com = new SqlCommand(selectQuery, con))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(com))
                    {
                        adapter.Fill(datatable);
                    }
                }
            }

            return datatable;
        }

        public DataTable FindByKeys(Dictionary<string, string> datas)
        {
            string value = "";
            int i = 0;
            foreach (var data in datas)
            {
                i++;
                if (i == datas.Count)
                {
                    value += data.Key + "=@" + data.Key;
                }
                else
                {
                    value += data.Key + "=@" + data.Key + "  or ";
                }
            }

            string selectQuery = "Select * from " + tableName + " where " + value;
            var datatable = new DataTable();
            using (SqlConnection con = new SqlConnection(connection))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(selectQuery, con))
                {
                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        foreach (var val in datas)
                        {
                            cmd.Parameters.AddWithValue('@' + val.Key, val.Value);
                        }

                        adapter.Fill(datatable);
                        int rows = cmd.ExecuteNonQuery();
                    }
                }
            }

            return datatable;
        }

        public bool Insert(Dictionary<String, String> datas)
        {
            string columns = "";
            string columnsVal = "";
            int rows;

            // Retourne les columns et leurs valeurs liées par des virgules
            int index = 0;
            foreach (var item in datas)
            {
                index++;
                if (index == datas.Count)
                {
                    columns += item.Key;
                    columnsVal += '@' + item.Key;
                }
                else
                {
                    columns += item.Key + ',';
                    columnsVal += '@' + item.Key + ',';
                }
            }

            string insertQuery = "Insert Into " + tableName + "(" + columns + ") Values (" + columnsVal + ")";

            using (SqlConnection con = new SqlConnection(connection))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(insertQuery, con))
                {
                    // Ajout automatique des données dans la bdd
                    foreach (KeyValuePair<string, string> val in datas)
                    {
                        cmd.Parameters.AddWithValue('@' + val.Key, val.Value);
                    }

                    rows = cmd.ExecuteNonQuery();
                }
            }
            return (rows > 0) ? true : false;
        }

        public bool Update(int id, Dictionary<String, String> datas)
        {
            int rows;
            int i = 0;
            string values = " set ";

            foreach (var val in datas)
            {
                i++;
                if (i == datas.Count)
                {
                    values += val.Key + "=@" + val.Key + " where id=" + id;
                }
                else
                {
                    values += val.Key + "=@" + val.Key + ',';
                }
            }

            using (SqlConnection con = new SqlConnection(connection))
            {
                con.Open();
                string updateQuery = "Update " + tableName + values;
                using (SqlCommand cmd = new SqlCommand(updateQuery, con))
                {
                    foreach (var val in datas)
                    {
                        cmd.Parameters.AddWithValue('@' + val.Key, val.Value);
                    }

                    rows = cmd.ExecuteNonQuery();
                }
            }
            return (rows > 0) ? true : false;
        }

        public bool Delete(int id)
        {
            int rows;
            string deleteQuery = "Delete from " + tableName + " where id=" + id;

            using (SqlConnection con = new SqlConnection(connection))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(deleteQuery, con))
                {
                    //com.Parameters.AddWithValue("id", id);
                    rows = cmd.ExecuteNonQuery();
                }
            }
            return (rows > 0) ? true : false;
        }
    }
}


